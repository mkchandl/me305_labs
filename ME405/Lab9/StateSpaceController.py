# -*- coding: utf-8 -*-
'''
@file       StateSpaceController.py
@brief      Contains StateSpaceController class
@details    

@author     Michelle Chandler
@author     Ben Sahlin
@date       March 10, 2020
'''

class StateSpaceController:
    '''
    '''
    def __init__ (self, k1, k2, k3, k4):    #later add in k1 for xdot and k3 for x
        '''
        @brief
        @details
        @param k2   Float representing full state feedback gain 2 which corresponds to thetadot
        @param k4   Float representing full state feedback gain 4 which corresponds to theta
        '''
        self.k1 = k1
        self.k2 = k2
        self.k3 = k3
        self.k4 = k4
        
    def calcTorque(self, xdot, thetaydot, x, thetay):   #note that for the other motor these are technically y's and thetax's
        '''
        '''
        #note: maybe it would be better to make this take in a list or tuple of the state variables
        #but I'm not sure it would make it easier
        Tx = -1*(self.k1*xdot+self.k2*thetaydot+self.k3*x+self.k4*thetay)
        return Tx
        
if __name__ == '__main__':
    # test code block
    # gains from platform only matlab with Ts = .5 sec and OS = 10%
    # k2 = -0.0225
    # k4 = -0.4124
    # gains from platform only matlab with Ts = 3 sec and OS = 5%
    k2 = 0.0008
    k4 = -0.0987
    ssc = StateSpaceController(k2, k4)
    print('torque is: {:}'.format(ssc.calcTorque(0,.08)))