# -*- coding: utf-8 -*-
'''
@file       FSM_BalancePlatform.py
@brief      Contains taskBalancePlatform class
@details    

@author     Michelle Chandler
@author     Ben Sahlin
@date       March 11, 2020
'''

import utime

class taskBalancePlatform:
    #Define states
    S0_Init     = 0
    S1_Zero     = 1
    S2_Balance  = 2
    S3_Stopped  = 3
    
    def __init__(self, EncoderDriver1, EncoderDriver2, MotorDriver, TouchPanelDriver, StateSpaceController, run_interval):    # later also controls touch panel driver
        '''
        @brief
        @details
        @param
        '''
        self.debug = True
        self.enc1 = EncoderDriver1          # controls encoder 1, thetay angle
        self.enc2 = EncoderDriver2          # controls encoder 2, thetax angle
        self.moe = MotorDriver              # controls both motors
        self.tp = TouchPanelDriver
        self.ssc = StateSpaceController     # state space controller object
        self.run_interval = run_interval    # microseconds
        self.current_time = utime.ticks_us()
        self.previous_run_time = self.current_time #maybe there's a better way to do this but on the first run it will just be s0_init anyways
        self.next_time = utime.ticks_add(self.current_time, self.run_interval)
        print('taskBalancePlatform object created')
        self.state = self.S0_Init
        # need to create these variables for data logging
        self.x = 0
        self.y = 0
        self.xdot = 0
        self.ydot = 0
        self.duty_requested1 = 0
        self.duty_requested2 = 0
        self.max_duty_change = 15        #max duty change per run
    
    def run(self):
        '''
        @brief
        @details
        '''
        self.current_time = utime.ticks_us()
        if utime.ticks_diff(self.current_time, self.next_time) > 0:
            # calculate actual time elapsed since last run, used for velocity calcs, adjust to seconds
            self.real_run_interval = utime.ticks_diff(self.current_time, self.previous_run_time)/int(1e6) #sec
            self.next_time = utime.ticks_add(self.current_time, self.run_interval)
            if(self.state == self.S0_Init):
                # initialize
                # on this first run through, 
                # note that previous_time will be incorrectly set equal to current_time
                self.transitionTo(self.S1_Zero)
            elif(self.state == self.S1_Zero):
                self.zero()
                self.moe.enable()
                self.moe.setDuty1(0)
                self.moe.setDuty2(0)
                self.transitionTo(self.S2_Balance)
            elif(self.state == self.S2_Balance):
                self.balance1()
                self.balance2()
            elif(self.state == self.S3_Stopped):
                print('Stopped')
            else:
                print('Invalid state code for taskBalancePlatform')
            # store the time this run started as the previous time for velocity calcs
            self.previous_run_time = self.current_time
            
# Ben: I just made some different functions instead of putting everything into the 
# run states so that it's easier to create adjusted functions for whether or not the 
# motors actually move, and to hopefully make balancing both x and y directions easier 
# to do simultaneously

    def zero(self):
        '''
        @brief
        @details
        '''
        
        print('Hold platform level. Zeroing in 3 seconds')
        utime.sleep_us(int(3*1e6))
        self.enc1.set_position(0)
        self.enc2.set_position(0)
        print('Platform zeroed. Current encoder tick positions of enc1: {:} and enc2: {:}'.format(self.enc1.get_position(), self.enc2.get_position()))
        
    def balance1(self):
        # motornumber==1: motor 1, encoder1, thetax, y direction on touch panel
        # motornumber==2: motor 2, encoder2, thetay, x direction on touch panel
        '''
        @brief
        @details
        @param motornumber Integer representing motor number to balance, can be either 1 or 2
        '''
        # update encoder, multiply by -1 because negative relation between platform angle and encoder angle
        # also for magnitude, theta_platform = 6/11*theta_encoder, see uploaded hand calcs
        theta_ticks = -6/11*self.enc1.update()                                 #ticks
        theta = self.enc1.ticks2rad(theta_ticks)                            #rad
        thetad_degs = -6/11*self.enc1.calc_velocity(self.real_run_interval)    #deg/s
        thetad = thetad_degs*3.1415/180                                    #rad/s
        
        #print('enc1 theta {:}, thetad {:}'.format(theta, thetad))
        
        # update touch panel to get x and xdot, also get y and ydot
        self.tp.getPos()        # updates TouchPanelDriver.pos value
        # self.tp.movingAvg()     # updates TouchPanelDriver.avg value
        if self.tp.pos[2] == False:
            # if touch panel does not detect touch, set x to 0 because then 
            # it will not effect torque requested from ssc equation
            self.x = 0
            self.y = 0
        else:
            # multiply by -1 because motor faces opposite direction as physically defined axis angle
            self.x = -1*self.tp.pos[0]/1000      # convert mm to m
            self.y = -1*self.tp.pos[1]/1000      # convert mm to m
        # the getVel function already checks to ensure it has 2 real points to use,
        # so no need to check if panel is detecting touch here. mm/s to m/s conversion 
        # is also done in getVel function
        self.xdot = -1*self.tp.getVel(self.real_run_interval)[0]
        self.ydot = -1*self.tp.getVel(self.real_run_interval)[1]
        
        #calculate desired duty cycle
        torque_requested = self.ssc.calcTorque(self.ydot, thetad, self.y, theta)
        self.duty_requested1 = int(self.moe.torque2duty(torque_requested))
        #print('xd: {:}, thetad: {:}, x: {:}, theta: {:}'.format(self.xdot, thetad, self.x, theta))
        #print('motor 1 duty requested {:}'.format(self.duty_requested1))
        if abs(self.duty_requested1-self.moe.duty1) > self.max_duty_change:
            if self.duty_requested1 < self.moe.duty1:
                self.duty_requested1 = self.moe.duty1-self.max_duty_change
            else:
                self.duty_requested1 = self.moe.duty1+self.max_duty_change
        
        #set duty
        self.moe.setDuty1(self.duty_requested1)
        #print('motor 1 is at duty of {:}'.format(self.moe.duty1))
        
    def balance2(self):
        '''
        @brief
        @details    This method must be called after .balance1() to work properly, 
                    because all state variables related to the touch panel (x, y, xdot, ydot), 
                    are updated in balance1 because we only want to call the touch 
                    panel update method one time, since it takes a long time.
        '''
        # update theta and thetad values for the second axis
        theta_ticks = -6/11*self.enc2.update()                                 #ticks
        theta = self.enc2.ticks2rad(theta_ticks)                            #rad
        thetad_degs = -6/11*self.enc2.calc_velocity(self.real_run_interval)    #deg/s
        thetad = thetad_degs*3.1415/180                                    #rad/s
        
        #calculate desired duty cycle
        torque_requested = self.ssc.calcTorque(self.xdot, thetad, self.x, theta)
        self.duty_requested2 = int(self.moe.torque2duty(torque_requested))
        #print('xd: {:}, thetad: {:}, x: {:}, theta: {:}'.format(self.xdot, thetad, self.x, theta))
        #print('motor 2 duty requested {:}'.format(self.duty_requested2))

        if abs(self.duty_requested2-self.moe.duty2) > self.max_duty_change:
            if self.duty_requested2 < self.moe.duty2:
                self.duty_requested2 = self.moe.duty2-self.max_duty_change
            else:
                self.duty_requested2 = self.moe.duty2+self.max_duty_change

        #set duty
        self.moe.setDuty2(self.duty_requested2)
        #print('motor 2 is at duty of {:}'.format(self.moe.duty2))
        print('xd: {:}, x: {:}, yd: {:}, y: {:}'.format(self.xdot, thetad, self.x, theta))
        print('motor 1 duty: {:.1f}, motor 2 duty: {:.1f}'.format(self.moe.duty1, self.moe.duty2))
    
    def transitionTo(self, next_state):
        '''
        @brief              Sets taskUI state to specified next state
        @param next_state   Variable representing the next state for the FSM
        '''
        self.state = next_state
        self.debugprint('CLController Switch to state '+ str(next_state))
        
    def debugprint(self, debugstring):
        '''
        @brief                  Prints string if debug parameter is true
        @details                Used for development purposes
        @param debugstring      String to print
        '''
        if self.debug == True:
            print(debugstring)