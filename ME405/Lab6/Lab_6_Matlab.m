%% ME 405 Term Project: Lab 6 
% This assignment will be a second step in preparation for the coming term
% project. In this lab we will develop a simulation and test a controller
% for the simplified model of the pivoting platform that was completed in Lab 0x05.

clc;
clearvars;
close all; 

%% Numerical Parameters
% provided by instructor
rm = .06;                % [mm]
lr = .050;                % [mm] 
rb = .0105;              % [mm] 
rg = .042;                % [mm]
lp = .110;               % [mm]
rp = .0325;              % [mm]
rc = .050;                % [mm] 
mb = .030;                % [g]
mp = .400;               % [g]
Ip = 0.00188;            % [kg*m^2]
b = 10;                 % [m*Nm*s/rad]
Ib_bar = 2/5*mb*rb^2;   % [kg*m^2]
g = 9.810;               % [m/s^2]


%% Jacobian Linearization

% Create symbolic variables and functions
syms theta x theta_d x_d T

% Step 1: Uncouple the system by pre-multiplying by M^?1. That is, q_dd= M^-1*f(q,q_d,u).
% M matrix comes from lab 5
M = [-(Ib_bar*rb+Ip*rb+mb*rb^3+mb*rb*rc^2+2*mb*rb^2*rc+mp*rb*rg^2+mb*rb*x^2)/rb,  -(mb*rb^2+mb*rc*rb+Ib_bar)/rb  ;
     -(mb*rb^3+mb*rc*rb^2+Ib_bar*rb)/rb                                        ,  -(mb*rb^2+Ib_bar)/rb            ];
% find the inverse of M
M_inv = inv(M);
% the f_matrix comes from lab 5
f_matrix = [b*theta_d-g*mb*(theta*(rb+rc)+x)+(T*lp/rm)+2*mb*theta_d*x*x_d-g*mp*rg*theta;
              -mb*rb*x*theta_d^2-g*mb*rb*theta];
q_dd = M_inv*f_matrix;
% q_dd is a 2x1 matrix with where q_dd(1,1) represents theta_dd and
% d_dd(2,1) represents x_dd

% Step 2: Augment the system by defining a state vector that includes both 
% the positions in q and the velocities in q_d. That is define a state vector as follows
% x_ = [q
%      q_d]
x_ = [theta; x; theta_d; x_d];
x_d_ = [theta_d; x_d; q_dd(1,:); q_dd(2,:)];

% Step 3:  linearize the system by use of a Jacobian linearization. The Jacobian 
% is defined as the partial derivative of a vector function with respect to 
% its vector argument. We can apply this notion along with the idea of a 
% Taylor series expansion to linearize our system about an operating point. 
% First we need a Jacobian for the states, x and a one for the inputs u

% functions for both state and input jacobian matrix
f1 = x_d_(1,:);
f2 = x_d_(2,:);
f3 = x_d_(3,:); 
f4 = x_d_(4,:);

% compute the state jacobian matrix of f1, f2 with respect to: x, theta, x_d,
% theta_d
jacob_state = jacobian([f1, f2, f3, f4], [theta x theta_d x_d]);

% compute the input jacobian matrix of f1, f2 with respect to: T
jacob_input = jacobian([f1, f2, f3, f4], [T]);

% Step 4: evaluating these matrices at a desired operating point x=x0 and 
% u=u0, we can come up with the A and B matrices for a standard statespace 
% representation. That is, A = Jacob_state when x0=u0=0 and B = Jacob_input
% when x0=u0=0 such that x_d_ = A*x_+B*u

%Create a symbolic function, js, and specify its formula by using a symbolic matrix, jacob_state.
js(theta, x, theta_d, x_d) = jacob_state;
% Plug in zeroes for the state variables in the jacobian state matrix
js_0 = js(0,0,0,0);

ju(T) = jacob_input;
% Plug in zeroes for the input variables in the jacobian input matrix
ju_0 = ju(0);

% The four new, linear system of equations to represent the system
f1_lin = js_0(1,1)*theta + js_0(1,2)*x + js_0(1,3)*theta_d + js_0(1,4)*x_d + ju_0(1,1)*T;
f2_lin = js_0(2,1)*theta + js_0(2,2)*x + js_0(2,3)*theta_d + js_0(2,4)*x_d + ju_0(2,1)*T;
f3_lin = js_0(3,1)*theta + js_0(3,2)*x + js_0(3,3)*theta_d + js_0(3,4)*x_d + ju_0(3,1)*T;
f4_lin = js_0(4,1)*theta + js_0(4,2)*x + js_0(4,3)*theta_d + js_0(4,4)*x_d + ju_0(4,1)*T;

%% Open-Loop Simulation

% Simulate the System given the following 4 cases: 
%    1) The ball is initially at rest on a level platform directly above the 
%       center of gravity of the platform and there is no torque input from
%       the motor. Run this simulation for 1 [s].
%           x = x_d = theta = T = 0 ,  t = 1s
%    2) The ball is initially at rest on a level platform offset horizontally 
%       from the center of gravity of the platform by 5 [cm] and there is no torque 
%       input from the motor. Run this simulation for 0.4 [s].
%           x = 5cm ,  xd = 0 ,  theta = 0 ,  T = 0 ,  t = 0.4s
%    3) The ball is initially at rest on a platform inclined at 5 degrees 
%       directly above the center of gravity of the platform and there is no torque
%       input from the motor. Run this simulation for 0.4 [s].
%           x = 0 ,  xd = 0 ,  theta = 5degrees ,  T = 0 , t = 0.4s
%    4) The ball is initially at rest on a level platform directly above the 
%       center of gravity of the platform and there is an impulse of 1 mNm*s 
%       applied by the motor. Run this simulation for 0.4 [s].
%           x = 0 ,  xd = 0 ,  theta = 0 ,  T_impulse = 1 ,  t = 0.4s



% CASE 1
% Initial Conditions
x0 = [0;0;0;0];
u0 = [0];
% Define Timespan
tspan = [0 1];
% Use ode45 to solve for x_dot
[ tout , xout ] = ode45 ( @(t,x) Lab6_case1(t,x,u0), tspan, x0);
figure(1)
% subplot for each state variable
subplot(4,1,1);
plot (tout,xout(:,1))
xlabel({'Time, t [s]'});
ylabel('theta [rad]');
subplot(4,1,2);
plot (tout,xout(:,2))
xlabel({'Time, t [s]'});
ylabel('x [mm]');
subplot(4,1,3);
plot (tout,xout(:,3))
xlabel({'Time, t [s]'});
ylabel('theta_d [rad/s]');
subplot(4,1,4);
plot (tout,xout(:,4))
xlabel({'Time, t [s]'
        ' '
        '\bfFigure 1: \rm Open-Loop Case 1'});
ylabel('x_d [mm/s]');

% CASE 2
% Initial Conditions
x0 = [0;50;0;0];
u0 = [0];
% Define Timespan
tspan = [0 0.4];
% Use ode45 to solve for x_dot
[ tout , xout ] = ode45 ( @(t,x) Lab6_case1(t,x,u0), tspan, x0);
figure(2)
subplot(4,1,1); % first plot is theta
plot (tout,xout(:,1))
xlabel({'Time, t [s]'});
ylabel('theta [rad]');
subplot(4,1,2); % second plot is x
plot (tout,xout(:,2))
xlabel({'Time, t [s]'});
ylabel('x [mm]');
subplot(4,1,3); % third plot is theta_d
plot (tout,xout(:,3))
xlabel({'Time, t [s]'});
ylabel('theta_d [rad/s]');
subplot(4,1,4); % fourth plot is x
plot (tout,xout(:,4))
xlabel({'Time, t [s]'
        ' '
        '\bfFigure 2: \rm Open-Loop Case 2'});
ylabel('x_d [mm/s]');

% CASE 3
% Initial Conditions
x0 = [rad2deg(5);0;0;0];
u0 = [0];
% Define Timespan
tspan = [0 0.4];
% Use ode45 to solve for x_dot
[ tout , xout ] = ode45 ( @(t,x) Lab6_case1(t,x,u0), tspan, x0);
figure(3)
subplot(4,1,1);
plot (tout,xout(:,1))
xlabel({'Time, t [s]'});
ylabel('theta [rad]');
subplot(4,1,2);
plot (tout,xout(:,2))
xlabel({'Time, t [s]'});
ylabel('x [mm]');
subplot(4,1,3);
plot (tout,xout(:,3))
xlabel({'Time, t [s]'});
ylabel('theta_d [rad/s]');
subplot(4,1,4);
plot (tout,xout(:,4))
xlabel({'Time, t [s]'
        ' '
        '\bfFigure 3: \rm Open-Loop Case 3'});
ylabel('x_d [mm/s]');

% CASE 4
% Initial Conditions
x0 = [0;0;0;0];
u0 = [10-10*.01];
% Define Timespan
tspan = [0 0.4];
% Use ode45 to solve for x_dot
[ tout , xout ] = ode45 ( @(t,x) Lab6_case1(t,x,u0), tspan, x0);
figure(4)
subplot(4,1,1);
plot (tout,xout(:,1))
xlabel({'Time, t [s]'});
ylabel('theta [rad]');
subplot(4,1,2);
plot (tout,xout(:,2))
xlabel({'Time, t [s]'});
ylabel('x [mm]');
subplot(4,1,3);
plot (tout,xout(:,3))
xlabel({'Time, t [s]'});
ylabel('theta_d [rad/s]');
subplot(4,1,4);
plot (tout,xout(:,4))
xlabel({'Time, t [s]'
        ' '
        '\bfFigure 4: \rm Open-Loop Case 4'});
ylabel('x_d [mm/s]');



%% Closed-Loop Simulation
% This will test the simulation in closed loop by implementing a regulator
% using a full state feedback. That is, [u] = -[K]*[x}. THe following gains
% are provided by the instructors: K = [−0.05 −0.02 −0.3 −0.2]. The units 
% for K1 through K4 are [N · s], [N · m · s], [N] and [N · m]
% respectively. The initial conditions from the open-loop simulation will
% be used again here. 
%  Run this simulation long enough for the state values to decay close to zero, at least 20 [s].

% Create the gain matrix
K = -[-0.2, -0.3, -0.02, -0.05];

% CASE 1
% Initial Conditions
x0 = [0;0;0;0];
u0 = K*x0;
% Define Timespan
tspan = [0 20];
% Use ode45 to solve for x_dot
[ tout_CL , xout_CL ] = ode45 ( @(t,x) lab6_closedloop(t,x,u0), tspan, x0);
figure(5)
% subplot for each state variable
subplot(4,1,1);
plot (tout_CL,xout_CL(:,1))
xlabel({'Time, t [s]'});
ylabel('theta [rad]');
subplot(4,1,2);
plot (tout_CL,xout_CL(:,2))
xlabel({'Time, t [s]'});
ylabel('x [mm]');
subplot(4,1,3);
plot (tout_CL,xout_CL(:,3))
xlabel({'Time, t [s]'});
ylabel('theta_d [rad/s]');
subplot(4,1,4);
plot (tout_CL,xout_CL(:,4))
xlabel({'Time, t [s]'
        ' '
        '\bfFigure 5: \rm Closed-Loop Case 1'});
ylabel('x_d [mm/s]');


% CASE 2
% Initial Conditions
x0 = [0;.05;0;0];
u0 = K*x0;
% Define Timespan
tspan = [0 20];
% Use ode45 to solve for x_dot
[ tout_CL , xout_CL ] = ode45 ( @(t,x) lab6_closedloop(t,x,u0), tspan, x0);
figure(6)
subplot(4,1,1); % first plot is theta
plot (tout_CL,xout_CL(:,1))
xlabel({'Time, t [s]'});
ylabel('theta [rad]');
subplot(4,1,2); % second plot is x
plot (tout_CL,xout_CL(:,2))
xlabel({'Time, t [s]'});
ylabel('x [mm]');
subplot(4,1,3); % third plot is theta_d
plot (tout_CL,xout_CL(:,3))
xlabel({'Time, t [s]'});
ylabel('theta_d [rad/s]');
subplot(4,1,4); % fourth plot is x
plot (tout_CL,xout_CL(:,4))
xlabel({'Time, t [s]'
        ' '
        '\bfFigure 6: \rm Closed-Loop Case 2'});
ylabel('x_d [mm/s]');


% CASE 3
% Initial Conditions
x0 = [rad2deg(5);0;0;0];
u0 = K*x0;
% Define Timespan
tspan = [0 20];
% Use ode45 to solve for x_dot
[ tout_CL , xout_CL ] = ode45 ( @(t,x) lab6_closedloop(t,x,u0), tspan, x0);
figure(7)
% subplot for each state variable
subplot(4,1,1);
plot (tout_CL,xout_CL(:,1))
xlabel({'Time, t [s]'});
ylabel('theta [rad]');
subplot(4,1,2);
plot (tout_CL,xout_CL(:,2))
xlabel({'Time, t [s]'});
ylabel('x [mm]');
subplot(4,1,3);
plot (tout_CL,xout_CL(:,3))
xlabel({'Time, t [s]'});
ylabel('theta_d [rad/s]');
subplot(4,1,4);
plot (tout_CL,xout_CL(:,4))
xlabel({'Time, t [s]'
        ' '
        '\bfFigure 7: \rm Closed-Loop Case 3'});
ylabel('x_d [mm/s]');

% CASE 4
% Initial Conditions
x0 = [0;0;0;0];
u0 = 10-10*.01;
% Define Timespan
tspan = [0 20];
% Use ode45 to solve for x_dot
[ tout_CL , xout_CL ] = ode45 ( @(t,x) lab6_closedloop(t,x,u0), tspan, x0);
figure(8)
% subplot for each state variable
subplot(4,1,1);
plot (tout_CL,xout_CL(:,1))
xlabel({'Time, t [s]'});
ylabel('theta [rad]');
subplot(4,1,2);
plot (tout_CL,xout_CL(:,2))
xlabel({'Time, t [s]'});
ylabel('x [mm]');
subplot(4,1,3);
plot (tout_CL,xout_CL(:,3))
xlabel({'Time, t [s]'});
ylabel('theta_d [rad/s]');
subplot(4,1,4);
plot (tout_CL,xout_CL(:,4))
xlabel({'Time, t [s]'
        ' '
        '\bfFigure 8: \rm Closed-Loop Case 4'});
ylabel('x_d [mm/s]');