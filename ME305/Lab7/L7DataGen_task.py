# -*- coding: utf-8 -*-
"""
@file L7DataGen_task.py

This file is the back end controller for lab 7. It has some trouble starting the
motors after the omega ref array is downloaded
Created on Mon Nov 16 11:59:43 2020

@author: Michelle Chandler
"""

import utime
from pyb import UART
import L7shares
import array

class DataGen_Task:
    '''
    @brief    Task for the encoder     
    @details  Task that calls update funct using class encoder driver object
                and then stores the most recent saved position and time values in 2 arrays
                filling up the arrays column by collumn each interation
                The storage is triggered by a command from the user, either G/S
    '''  
    ## Constant defining State 0 - Initialization
    S0_INIT                  = 0    
    
    ## Waiting for gain
    S1_WAIT_FOR_GAIN         = 1
    
    ## Waiting for omega ref array
    S2_DOWNLOAD_OMEGA_REF    = 2 
    
    ## Data collection and motor control state
    S3_CALL_UPDATE           = 3
    
    ## Sending data back to front end    
    S4_PRINT_RESP            = 4
    
    def __init__(self, interval, EncDriver, Loop, MotDriver): 
        '''
        @brief    for the data generation/ storage class
        @param Driver is a Class Encoder Driver object
        '''  
        
        ## The serial communication set up
        self.myuart = UART(2)
        #self.myuart = pyb.uart(1, 9600)                         # init with given baudrate
        #self.myuart.init(9600, bits=8, parity=None, stop=1) # init with given parameters

        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ##  The amount of time in mili seconds between runs of the task
        #self.interval = interval
        self.interval = int(interval*1e6)
        L7shares.interval = self.interval
        
        self.EncDriver = EncDriver
        
        self.Loop = Loop
        
        self.MotDriver = MotDriver
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        #self.start_time = time.time()
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        #self.next_time = self.start_time + self.interval
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ## The number of values placed into the position and time array
        ## used to determine when to start sending array data to computer 
        self.val_num = 0 
        
        # The 'timestamp' for when the task was run
        L7shares.time = utime.ticks_add(self.val_num, self.interval)
        
        self.omega_ref = array.array('f', []) #rpm
        
        # L6shares.gain = .08
        
        

        
    def run(self): 
        '''
        @brief runs one iteration of the task where class encoder driver update method is called
        '''
        
        #self.curr_time = time.time()
        self.curr_time = utime.ticks_us()
        # print('Curr_time: ' + str(self.curr_time))
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
                    
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                # print('State 0')
                self.transitionTo(self.S1_WAIT_FOR_GAIN)
                
                
            elif(self.state == self.S1_WAIT_FOR_GAIN):
                # Run State 1 Code
                # L7shares.gain = 2.2
                # self.transitionTo(self.S2_DOWNLOAD_OMEGA_REF)
                # check for gain sent from front end
                self.myuart.any()
                if self.myuart.any() != 0:
                    # save the values read 
                    self.gain_string = self.myuart.readline()
                    # save value as gain after making it a float
                    L7shares.gain = float(self.gain_string)
                    self.Loop.set_Kp(L7shares.gain)
                    self.transitionTo(self.S2_DOWNLOAD_OMEGA_REF)

        
            elif(self.state == self.S2_DOWNLOAD_OMEGA_REF):
                # Run State 2 Code
                # Check for any characters sent by front end
                self.myuart.any()
                # While there are characters to be read
                if self.myuart.any() != 0:
                    self.velocity_string = self.myuart.readline()
                    # Read add these values to the omega ref array for use in state 3
                    self.omega_ref.append(float(self.velocity_string))
                elif self.myuart.any() == 0:
                    self.transitionTo(self.S3_CALL_UPDATE)
                    # Restart the val_num variable 
                    self.val_num = 0
                    # counter to track when the  full omega ref array has been used
                    self.ref_count = 0
                    # enable motor 
                    self.MotDriver.enable()
                    print('State 3: data collection')

                  
            elif(self.state == self.S3_CALL_UPDATE):
                # Run State 3 Code
                # Each run the ref count is increased, this continues until all omega ref values hve been used
                if self.ref_count < len(self.omega_ref):
                    print('updating encoder position')
                    # Updating encoder position
                    self.EncDriver.update(L7shares.delta_rpm, L7shares.position, self.interval)
                    # Adding new velocity value to the velocity array
                    L7shares.resp_v.append(L7shares.delta_rpm)
                    # Adding new position value to the postion array
                    L7shares.resp_p.append(L7shares.position)
                    print('getting new duty using closed loop')
                    # Obtaining the new duty using the closed loop object loop
                    L7shares.L = self.Loop.update(L7shares.delta_rpm, self.omega_ref[self.ref_count], L7shares.gain)
                    print('new duty:' + str(L7shares.L))
                    # Using the newly obtained duty to set the motor speed
                    self.MotDriver.set_duty(L7shares.L)
                    # COunting up the ref_count variable
                    self.ref_count += 1
                    print('ref count: ' + str(self.ref_count))
                elif self.ref_count == len(self.omega_ref):
                    print('done filling array, going to S3')
                    # Once all velocity reference values have been used, transitions to the print response state
                    self.transitionTo(self.S4_PRINT_RESP)
                    # Turn off motor
                    self.MotDriver.disable()
                    self.n = 0
                    print('State 4: print')
                
                    
            elif(self.state == self.S4_PRINT_RESP):    
                # Run State 3 Code
                # print('rep_v: ' + str(L7shares.resp_v))
                # print('rep_p: ' + str(L7shares.resp_p))
                # Sending the position and velocity arrays back to the front end 
                if self.n < len(L7shares.resp_v):
                    self.myuart.write('{:}, {:}\r\n'.format(L7shares.resp_p[self.n], L7shares.resp_v[self.n]))
                    print(L7shares.resp_v[self.n], L7shares.resp_v[self.n])
                    self.n = self.n + 1
                elif self.n > len(L7shares.resp_v):
                        print('stopping print')
                        # Start over
                        self.transitionTo(self.S0_INIT)
                
                    
                
            else:
                # Invalid state code (error handling)
                pass
                    
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)
            # specifying the next value of the variable time that will be saved in the time array
            L7shares.time = utime.ticks_add(self.val_num, self.interval)
            self.val_num += 1
        
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState   