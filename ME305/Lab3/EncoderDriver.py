# -*- coding: utf-8 -*-
"""
@file EncoderDriver
Created on Fri Oct 16 16:21:37 2020

@author: CCHAN
"""
import Lab3shares
class Encoder_Driver:
    '''
    @brief A Class that drives the encoder
    @details The Ecoder Driver class is in charge of keeping track of the 
             position of the motor and sometimes resetting it
    '''
    
    def __init__(self, tim, pinA6, pinA7, period):
        '''
        @brief Creates an Encoder_Driver object
        @param tim      timer from pyb
        @param pinA6    pin on nucleo
        @param pinA7    pin on nucleo
        '''
        self.period = period
        #self.period = 0xFFFF
        
        ## 
        self.tim = tim
        
        ## Pin object used for Channel 1
        self.pinA6 = pinA6
         
        
        ## Pin object used for Channel 2 
        self.pinA7 = pinA7
         
        
        ## Initialize variable curr_count
        self.curr_count = self.tim.counter()
            
        ## Initialize the position variable
        Lab3shares.position = self.curr_count
        
    def update(self):  
        '''
        @brief Updates position variable, handles bad deltas
        '''
        ## This method is called regularly to update the recorded position of the encoder
        ## assign the value returned by tim.counter to variable count
        self.last_count = self.curr_count
        self.curr_count = self.tim.counter()
        
        ## variable finding the delta between the two most recent counts
        Lab3shares.delta = self.curr_count - self.last_count
        
        ## Deals with bad deltas
        if Lab3shares.delta > self.period/2:
            Lab3shares.delta -= self.period
                
        elif Lab3shares.delta < -self.period/2:
            Lab3shares.delta += self.period
            
        Lab3shares.position += Lab3shares.delta
        
        print(str(Lab3shares.position))
        
    def get_position(self): 
        '''
        @brief This method returns the most recently updated position of the encoder
        '''
        print(str(Lab3shares.position))
        
    def set_position(self):
        '''
        @brief Resets the position variable to zero
        '''
        self.position = 0
        print(str(Lab3shares.position))
        
    def get_delta(self):
        '''
        @brief Calculates delta and corrects bad deltas
        '''
        print(Lab3shares.delta)