# -*- coding: utf-8 -*-
"""
@file Lab3.py
# ampy --port COM3 put Lab3main.py Lab3main.py 
Created on Tue Oct 13 13:12:25 2020

@author: CCHAN
"""
import pyb 
import Lab3shares
import utime
import EncoderDrive
import Lab3Main

class Encoder_Task:
    '''
    @brief    Task for the encoder     
    @details  task representing the encoder which is driven by the class encoder
              driver
    '''  
    ## Constant defining State 0 - Initialization
    S0_INIT     = 0    
    
    ## Constant defining State 1
    S1          = 1    
    
    def __init__(self, interval, Driver): 
        '''
        @brief      
        '''  
        
        ## The encoder driver object that updates the position of the encoder
        self.Driver = Driver
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ##  The amount of time in seconds between runs of the task
        #self.interval = interval
        self.interval = int(interval*1e6)
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        #self.start_time = time.time()
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        #self.next_time = self.start_time + self.interval
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
         
        
        
    def run(self): 
        '''
        @brief runs one iteration of the task where class encoder driver update method is called
        '''
        #self.curr_time = time.time()
        self.curr_time = utime.ticks_us()
        if utime.ticks_diff(self.curr_time, self.next_time) > 0:
                    
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                self.transitionTo(self.S1)
            
            elif(self.state == self.S1):
                # Run State 1 Code
                Driver.update()
                if Lab3shares.cmd:
                    Lab3shares.resp = self.handle_input(lab3shares.cmd)
                    Lab3shares.cmd = None
                    
            else:
                # Invalid state code (error handling)
                pass
                    
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)
        
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState   
            
    def handle_imput(self, cmd):
        if cmd > 111 and cmd < 113:
            Driver.set_position()
        
        elif cmd > 79 and cmd < 81:
            Driver.get_position()
            
        elif cmd > 67 and cmd < 69:
            Driver.get_delta()
        
        else:
            pass