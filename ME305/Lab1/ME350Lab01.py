''' 
@file ME350Lab01.py

@author Michelle Chandler

@date September 26, 2020

Link to Source Code Can be found here: https://bitbucket.org/mkchandl/me305_labs/src/master/Lab1/ME350Lab01.py

 '''
 
 
 

from numpy import zeros




def fib (idx):
    '''
    @brief   A function for calculating specific fibonacci sequence number from a given index value
    @details The function recieves an index number as an input and and returns 
             the fibonacci number that corresponds to the index value. 
    '''
    print ('Calculating Fibonacci number at index n = {:}.'.format(idx))
    thing = zeros([idx + 1])
    ind = idx
    if isinstance(ind, int)==False:
        # Rejects inputs that are not integers
        print('Please input an integer')
    elif isinstance(ind, str)==True:
        # Rejects inputs that are strings
        print('Please input an integer, not a string')
    elif idx < 0:
        # Rejects inputs that aren't positive
        print('Please enter a positive integer')
    else: 
        # If the input is a positive integer it passes on to this nested loop
        if ind == 0:
            thing[0]=0 
        elif ind == 1:
            thing[0]=0
            thing[1]=1
        else:
            thing[0]=0
            thing[1]=1 
            n = 2
            while n <= idx: 
                thing[n]=thing[n-1]+thing[n-2]
                n = n+1
    print(thing[idx])

#if __name__ == '__main__':
 #  fib(8)
