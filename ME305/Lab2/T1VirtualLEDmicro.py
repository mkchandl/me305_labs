# -*- coding: utf-8 -*-
"""
Created on Mon Oct 12 13:55:12 2020

@author: CCHAN
"""

from random import choice
import utime
import pyb

class TaskLED:
    '''
    @brief      A finite state machine to control an LED.
    @details    This class implements a finite state machine to control the
                operation of an LED.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT             = 0    
    
    ## Constant defining State 1
    S1_LED_ON           = 1    
    
    ## Constant defining State 2
    S2_LED_OFF          = 2    
    
    ## Constant defining State 5
    S3_DO_NOTHING       = 3
    
    def __init__(self, interval, LEDon, LEDoff, LED):
        '''
        @brief            Creates a TaskWindshield object.
        @param GoButton   An object from class Button representing on/off
        @param LeftLimit  An object from class Button representing the left Limit switch
        @param RightLimit An object from class Button representing the right Limit switch
        @param Motor      An object from class MotorDriver representing a DC motor.
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## The button object used for the go command
        self.LEDon = LEDon
        
        ## The button object used for the left limit
        self.LEDoff = LEDoff
        
        ## The motor object "wiping" the wipers
        self.LED = LED
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ##  The amount of time in seconds between runs of the task
        self.interval = int(interval*1e6)
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        
        self.curr_time = utime.ticks_us()
        if self.curr_time > self.next_time:
            
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                self.transitionTo(self.S2_LED_OFF)
                self.LED.Off()
                print(str(self.runs) + ': State 0 ' + str(self.curr_time-self.start_time))
            
            elif(self.state == self.S1_LED_ON):
                # Run State 1 Code
                
                if(self.LEDon.getButtonState()):
                    self.transitionTo(self.S2_LED_OFF)
                    self.LED.Off()
                print(str(self.runs) + ': State 1 ' + str(self.curr_time-self.start_time))
            
            elif(self.state == self.S2_LED_OFF):
                # Run State 2 Code
                self.transitionTo(self.S1_LED_ON)
                self.LED.On()
                print(str(self.runs) + ': State 2 ' + str(self.curr_time-self.start_time))
            
            else:
                # Invalid state code (error handling)
                pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = self.next_time + self.interval
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        

class Button:
    '''
    @brief      A pushbutton class
    @details    This class represents a button that the can be pushed by the
                imaginary driver to turn on or off the wipers. As of right now
                this class is implemented using "pseudo-hardware". That is, we
                are not working with real hardware IO yet, this is all pretend.
    '''
    
    def __init__(self, pin):
        '''
        @brief      Creates a Button object
        @param pin  A pin object that the button is connected to
        '''
        
        ## The pin object used to read the Button state
        self.pin = pin
        
        print('Button object created attached to pin '+ str(self.pin))

    
    def getButtonState(self):
        '''
        @brief      Gets the button state.
        @details    Since there is no hardware attached this method
                    returns a randomized True or False value.
        @return     A boolean representing the state of the button.
        '''
        return choice([True, False])

class PowerSupply:
    '''
    @brief      A motor driver.
    @details    This class represents a motor driver used to make the wipers
                wipe back and forth.
    '''
    
    def __init__(self, pinA5):
        '''
        @brief Creates a MotorDriver Object
        '''
        self.pinA5 = pinA5
    
    def On(self):
        '''
        @brief Turns on LED
        '''
        print('Power to LED')
        self.pinA5.high()
    
    def Off(self):
        '''
        @brief Turns off LED
        '''
        print('No Power to LED')
        self.pinA5.low()

pinA5 = pyb.Pin(pyb.Pin.cpu.A5, pyb.Pin.OUT_PP) 

LEDon1 = Button('PB7')
LEDoff1 = Button('PB8')
LED1 = PowerSupply(pinA5)

LEDon2 = Button('PB7')
LEDoff2 = Button('PB8')
LED2 = PowerSupply(pinA5)



Task1 = TaskLED(0.1, LEDon1, LEDoff1, LED1)
Task2 = TaskLED(0.1, LEDon2, LEDoff2, LED2)

while True:
#for N in range(10000): # effectively while(True):
    Task1.run()
    Task2.run()






















