''' 
@file MotorDriver.py

Motor Driver for labs 6 and 7

@author: Michelle Chandler

'''

import pyb

class Motor_Driver:
    ''' 
    This class implements a motor driver for the
    ME405 board. 
    '''

    def __init__(self, sleep, IN1_pin, IN2_pin, tim):
        ''' 
        @brief Creates a motor driver by initializing GPIO pins and turning the motor off for safety.
        @param nSLEEP_pin A pyb.Pin object to use as the enable pin.
        @param IN1_pin A pyb.Pin object to use as the input to half bridge 1.
        @param IN2_pin A pyb.Pin object to use as the input to half bridge 2.
        @param timer A pyb.Timer object to use for PWM generation on
        IN1_pin and IN2_pin. 
        '''
        # print ('Creating a motor driver')
        
        self.sleep = sleep
        # self.nSLEEP_pin = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)
        self.sleep.low()
        
        self.IN1_pin = IN1_pin
        # self.IN1_pin = pyb.Pin(pyb.Pin.cpu.B4)
        
        self.IN2_pin = IN2_pin
        # self.IN2_pin = pyb.Pin(pyb.Pin.cpu.B5)
        
        self.tim = tim
        # self.timer = pyb.Timer(3, freq=20000)
        self.t3ch1 = self.tim.channel(1, pyb.Timer.PWM, pin = self.IN1_pin)
        self.t3ch2 = self.tim.channel(2, pyb.Timer.PWM, pin = self.IN2_pin)

    def enable(self):
        '''
        @brief This method sets the motor sleep pin high
        '''
        # print ('Enabling Motor')
        # sets pinA15 to high
        self.sleep.high()

    def disable(self):
        '''
        @brief This method sets the motor sleep pin low
        '''        
        print ('Disabling Motor')
        # sets pinA15 to low
        self.sleep.low()

    def set_duty (self, duty):
        ''' 
        @brief This method sets the duty cycle to be sent
        @details to the motor to the given level. Positive values
        cause effort in one direction, negative values
        in the opposite direction.
        @param duty A signed integer holding the duty
        cycle of the PWM signal sent to the motor 
        '''
        # if, elif statement controls the direction of the motor
        # print('In set duty method')
        if duty > 0:
            if duty > 100:
                duty = 100
                self.t3ch1.pulse_width_percent(0)
                self.t3ch2.pulse_width_percent(duty)
            else:
                self.t3ch1.pulse_width_percent(0)
                self.t3ch2.pulse_width_percent(duty)
            
        elif duty < 0:
            if duty < -100:
                duty = 100
                self.t3ch2.pulse_width_percent(0)
                self.t3ch1.pulse_width_percent(duty)
            else:
                duty = abs(duty)
                self.t3ch2.pulse_width_percent(0)
                self.t3ch1.pulse_width_percent(duty)  
                
    

if __name__ == '__main__':
    # Adjust the following code to write a test program for your motor class. Any
    # code within the if __name__ == '__main__' block will only run when the
    # script is executed as a standalone program. If the script is imported as
    # a module the code block will not run.

    # Create the pin objects used for interfacing with the motor driver
    print('setting up pins')
    sleep = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP);
    IN1_pin = pyb.Pin(pyb.Pin.cpu.B4);
    IN2_pin = pyb.Pin(pyb.Pin.cpu.B5);

    # Create the timer object used for PWM generation
    tim = pyb.Timer(3, freq=20000);

    # Create a motor object passing in the pins and timer
    moe = Motor_Driver(sleep, IN1_pin, IN2_pin, tim)
    
    moe.enable()
    while True:
        for n in range(20):
            moe.set_duty(40)
    moe.disable()

