# -*- coding: utf-8 -*-
"""
@file DataGen_task.py

Backend task/ controller for the motor of lab 6
Created on Mon Nov 16 11:59:43 2020

@author: Michelle Chandler
"""

import utime
from pyb import UART
import L6shares

class DataGen_Task:
    '''
    @brief    Task for the encoder     
    @details  Task that calls update funct using class encoder driver object
                and then stores the most recent saved position and time values in 2 arrays
                filling up the arrays column by collumn each interation
                The storage is triggered by a command from the user, either G/S
    '''  
    ## Constant defining State 0 - Initialization
    S0_INIT     = 0    
    
    ## Waitign for character state
    S1_WAIT_FOR_CMD    = 1
    
    ## Waiting for response state
    S2_CALL_UPDATE    = 2 
    
    S3_PRINT_RESP     = 3
    
    def __init__(self, interval, EncDriver, Loop, MotDriver): 
        '''
        @brief    for the data generation/ storage class
        @param Driver is a Class Encoder Driver object
        '''  
        
        ## The serial communication set up
        self.myuart = UART(2)
        #self.myuart = pyb.uart(1, 9600)                         # init with given baudrate
        #self.myuart.init(9600, bits=8, parity=None, stop=1) # init with given parameters

        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ##  The amount of time in mili seconds between runs of the task
        #self.interval = interval
        self.interval = int(interval*1e6)
        L6shares.interval = self.interval
        
        self.EncDriver = EncDriver
        
        self.Loop = Loop
        
        self.MotDriver = MotDriver
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        #self.start_time = time.time()
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        #self.next_time = self.start_time + self.interval
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ## The number of values placed into the position and time array
        ## used to determine when to start sending array data to computer 
        self.val_num = 0 
        
        # The 'timestamp' for when the task was run
        L6shares.time = utime.ticks_add(self.val_num, self.interval)
        
        self.omega_ref = 300 #rpm
        
        # L6shares.gain = .08
        

        
    def run(self): 
        '''
        @brief runs one iteration of the task where class encoder driver update method is called
        '''
        
        #self.curr_time = time.time()
        self.curr_time = utime.ticks_us()
        # print('Curr_time: ' + str(self.curr_time))
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
                    
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                # print('State 0')
                self.transitionTo(self.S1_WAIT_FOR_CMD)
                self.runs = 0
                L6shares.gain = 0
                
                
            elif(self.state == self.S1_WAIT_FOR_CMD):
                # Run State 1 Code
                # print('State 1')
                # L6shares.gain = .3
                # self.val_num = 0
                # self.MotDriver.enable()
                # print('enabled motor')
                # self.transitionTo(self.S2_CALL_UPDATE)
                self.myuart.any()
                if self.myuart.any() != 0:
                    # if L6shares.cmd:
                    # print('user input recieved')
                    self.gain_string = self.myuart.readline()
                    # L6shares.gain = self.gain_string.strip() # strip any special characters 
                    L6shares.gain = float(self.gain_string)
                    self.Loop.set_Kp(L6shares.gain)
                    # print('gain: ' + str(L6shares.gain))
                    # print('transitioning to S2')
                    self.transitionTo(self.S2_CALL_UPDATE)
                    self.val_num = 0
                    self.MotDriver.enable()
               

                # else:
                #     # print('no user input detected')
                #     self.transitionTo(self.S1_WAIT_FOR_CMD)
               
                    
                    
            elif(self.state == self.S2_CALL_UPDATE):
                # Run State 2 Code
                # print('State 2')
                self.EncDriver.update(L6shares.delta, L6shares.delta_rpm, L6shares.position, self.interval)
                L6shares.resp_v.append(L6shares.delta_rpm)
                L6shares.resp_t.append(L6shares.time)
                L6shares.L = self.Loop.update(L6shares.delta_rpm, self.omega_ref, L6shares.gain)
                L6shares.resp_L.append(L6shares.L)
                self.MotDriver.set_duty(L6shares.L)
                # print(' added to array')
                if self.val_num == 200:
                    # print('done filling array, going to S3')
                    self.transitionTo(self.S3_PRINT_RESP)
                    self.MotDriver.disable()
                    self.n = 0
                    
            elif(self.state == self.S3_PRINT_RESP):
                # print('State 3')
                # Run State 3 Code
                # print('rep_v: ' + str(L6shares.resp_v))
                # print('rep_t: ' + str(L6shares.resp_t))
                # print('rep_L: ' + str(L6shares.resp_L))
                if self.n < len(L6shares.resp_t):
                    self.myuart.write('{:}, {:}\r\n'.format(L6shares.resp_t[self.n], L6shares.resp_v[self.n]))
                    # print(L6shares.resp_t[self.n], L6shares.resp_v[self.n])
                    self.n = self.n + 1
                elif self.n > len(L6shares.resp_t):
                        print('stopping print')
                        self.transitionTo(self.S0_INIT)
                
                    
                
            else:
                # Invalid state code (error handling)
                pass
                    
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)
            # specifying the next value of the variable time that will be saved in the time array
            L6shares.time = utime.ticks_add(self.val_num, self.interval)
            self.val_num += 1
        
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState   