# -*- coding: utf-8 -*-
"""
@file EncoderDriver.py
Created on Fri Oct 16 16:21:37 2020

@author: CCHAN
"""
import L6shares

class Encoder_Driver:
    '''
    @brief A Class that drives the encoder
    @details The Ecoder Driver class is in charge of keeping track of the 
             position of the motor and sometimes resetting it
    '''
    
    def __init__(self, tim, pinB6, pinB7, period):
        '''
        @brief Creates an Encoder_Driver object
        @param tim      timer from pyb
        @param pinA6    pin on nucleo
        @param pinA7    pin on nucleo
        '''
        self.period = period
        #self.period = 0xFFFF
        
        ## 
        self.tim = tim
        
        ## Pin object used for Channel 1
        self.pinB6 = pinB6
         
        
        ## Pin object used for Channel 2 
        self.pinB7 = pinB7
         
        
        ## Initialize variable curr_count
        self.curr_count = self.tim.counter()
            
        ## Initialize the position variable
        L6shares.position = self.curr_count
        L6shares.resp_v.append(L6shares.position)
        L6shares.delta = 0
        
    def update(self):  
        '''
        @brief Updates position variable, handles bad deltas
        '''
        # print('calling enocder driver update')
        
        ## This method is called regularly to update the recorded position of the encoder
        ## assign the value returned by tim.counter to variable count
        self.last_count = self.curr_count
        self.curr_count = self.tim.counter()
        
        ## variable finding the delta between the two most recent counts
        L6shares.delta = self.curr_count - self.last_count
        
        ## Deals with bad deltas
        if L6shares.delta > self.period/2:
            L6shares.delta -= self.period
                
        elif L6shares.delta < -self.period/2:
            L6shares.delta += self.period

        ## saves the updated position
        L6shares.position += L6shares.delta
        
        ## Unit conversion for delta
        L6shares.delta_rpm = int((L6shares.delta*60000)/(4000*10))
        
    def get_position(self): 
        '''
        @brief This method returns the most recently updated position of the encoder
        '''
        print(str(L6shares.position))
        
    def set_position(self):
        '''
        @brief Resets the position variable to zero
        '''
        L6shares.position = 0
        print(str(L6shares.position))
        
    def get_delta(self):
        '''
        @brief Calculates delta and corrects bad deltas
        '''
        print(L6shares.delta)
        

